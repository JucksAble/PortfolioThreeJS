import * as THREE from "three";
import Experience from "../experience.js";
import GSAP from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger.js";
import ASScroll from '@ashthornton/asscroll';

export default class Controls {
    constructor() {
        this.experience = new Experience();
        this.scene = this.experience.scene;
        this.sizes = this.experience.sizes;
        this.resources = this.experience.resources;
        this.time = this.experience.time;
        this.camera = this.experience.camera;
        this.room = this.experience.world.room.actualRoom;
        this.sun = this.experience.world.environment.sunLight;
        this.circleFirst = this.experience.world.floor.circleFirst;
        this.circleSecond = this.experience.world.floor.circleSecond;
        this.circleThird = this.experience.world.floor.circleThird;
        GSAP.registerPlugin(ScrollTrigger);
        this.mm = GSAP.matchMedia();

        this.setSmoothScroll();
        this.setScrollTrigger();
    }
    
    setupASScroll() {
        // https://github.com/ashthornton/asscroll
        const asscroll = new ASScroll({
            ease: 0.13,
            disableRaf: true
        });


        GSAP.ticker.add(asscroll.update);
        
        ScrollTrigger.defaults({
            scroller: asscroll.containerElement
        });
        
        
        ScrollTrigger.scrollerProxy(asscroll.containerElement, {
            scrollTop(value) {
                if (arguments.length) {
                    asscroll.currentPos = value;
                    return;
                }
                return asscroll.currentPos;
            },
            getBoundingClientRect() {
                return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
            },
            fixedMarkers: true
        });
        
        
        asscroll.on("update", ScrollTrigger.update);
        ScrollTrigger.addEventListener("refresh", asscroll.resize);
        
        requestAnimationFrame(() => {
            asscroll.enable({
                newScrollElements: document.querySelectorAll(".gsap-marker-start, .gsap-marker-end, [asscroll]")
            });
            
        });
        return asscroll;
    }
    
    setSmoothScroll() {
        this.asscroll = this.setupASScroll();
    }
    setScrollTrigger() {
        //Desktop
        this.mm.add("(min-width: 969px)", () => {
            this.room.scale.set(1, 1, 1);

            // First section 
            this.firstMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".first-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.firstMoveTimeline.to(this.room.position, {
                x: () => {
                    return this.sizes.width * 0.00135;
                },
            });

            // Second section 
            this.secondMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".second-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.secondMoveTimeline.to(this.room.position, {
                x: () => {
                    return this.sizes.height * 0.0002;
                },
                z: () => {
                    return this.sizes.height * 0.009;
                },
            }, "same");
            this.secondMoveTimeline.to(this.room.scale, {
                x: 4,
                y: 4,
                z: 4,
            }, "same");
            this.secondMoveTimeline.to(this.sun.position, {
                x: -1.5,
                y: 15,
                z: 12,
            }, "same");
            // Third section 
            this.thirdMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".third-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.thirdMoveTimeline.to(this.room.position, {
                x: () => {
                    return this.sizes.width * 0.00135;
                },
                z: () => {
                    return this.sizes.height * 0;
                },
            }, "same2");
            this.thirdMoveTimeline.to(this.room.scale, {
                x: 1,
                y: 1,
                z: 1,
            }, "same2");
            this.secondMoveTimeline.to(this.sun.position, {
                x: -1.5,
                y: 7,
                z: 3,
            }, "same2");
        });

        //Mobile
        this.mm.add("(max-width: 968px)", () => {
            this.room.scale.set(0.6, 0.6, 0.6);
            this.room.position.set(0, 0, 0);

            // First section 
            this.firstMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".first-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.firstMoveTimeline.to(this.room.scale, {
                y: 1,
                z: 1,
                x: 1,
            });

            // Second section 
            this.secondMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".second-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.secondMoveTimeline.to(this.room.scale, {
                y: 3,
                z: 3,
                x: 3,
            }, "sameM");
            this.secondMoveTimeline.to(this.room.position, {
                x: () => {
                    return this.sizes.height * 0.0022;
                },
                z: () => {
                    return this.sizes.height * 0.006;
                },
            }, "sameM");

            // Third section 
            this.thirdMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".third-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.thirdMoveTimeline.to(this.room.position, {
                x: () => {
                    return this.sizes.width * 0;
                },
                z: () => {
                    return this.sizes.height * 0;
                },
            }, "sameM2");
            this.thirdMoveTimeline.to(this.room.scale, {
                x: 0.7,
                y: 0.7,
                z: 0.7,
            }, "sameM2");
            this.secondMoveTimeline.to(this.sun.position, {
                x: -1.5,
                y: 7,
                z: 3,
            }, "sameM2");
        });

        this.mm.add("(min-width: 0px)", () => {
            // First section 
            console.log(this.circleFirst);
            this.firstMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".first-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.firstMoveTimeline.to(this.circleFirst.scale, {
                x: 3,
                y: 3,
                z: 3,
            });

            // Second section 
            this.secondMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".second-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.secondMoveTimeline.to(this.circleSecond.scale, {
                x: 3,
                y: 3,
                z: 3,
            });
            // Third section 
            this.thirdMoveTimeline = new GSAP.timeline({
                scrollTrigger: {
                    trigger: ".third-move",
                    start: "top top",
                    end: "bottom bottom",
                    scrub: 0.6,
                    invalidateOnRefresh: true,
                },
            });
            this.thirdMoveTimeline.to(this.circleThird.scale, {
                x: 3,
                y: 3,
                z: 3,
            });
        })
    }

    resize() { }

    update() {

    }
}